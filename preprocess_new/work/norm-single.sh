#!/bin/bash

mkdir -p temp

#The first 290 is usually 
cat $1 | sed '/^\*/d' | sed 's|\r||g' | sed 's|^[0-9]*||g' |  awk ' NR > 320 { if ($1 != "Chapter" && $1 != "CHAPTER" && $1 != "Part" && $1 != "PART") printf("%s ", $0); } ' > temp/one-sentence
cat temp/one-sentence | sed -e "s|[A-Z]\.| |g" | tr [:upper:] [:lower:] | sed -e "s|mr\.|mr|g" -e "s|mrs\.|mrs|g" -e "s|dr\.|dr|g" -e "s|st\.|st|g" > temp/one-sentence-2
cat temp/one-sentence-2 | sed -e 's|--||g' -e 's|[0-9][0-9,.]*|NNN|g' -e "s|, *\"|. \"|g" -e "s|\. \. \.|\.|g" | sed "s|\[[^\[]*\]||g" | sed "s|[()]| |g"  > temp/one-sentence-3
cat temp/one-sentence-3 | sed 's|`| |g' | sed "s|^'||g" | sed "s|\^NNN||g" | sed -e 's|"| |g' -e 's|,| <comma> |g' -e 's|;| <semi> |g' -e 's|[:?!]|\.|g' | sed 's|\.|\n|g' > $2
echo ' ' >> $2

