#!/bin/bash

cat normed/normed_all | awk ' { for (i=1;i<=NF;i++) cc[$i]=cc[$i]+1; } END { for (key in cc) print key,cc[key]; } ' | sort -r -n -k2 > normed/normed_all.vocaball
cat normed/normed_all.vocaball | head -29998 | awk ' { print $1 } ' > normed/normed_all.vocab30000
echo "</s>" >> normed/normed_all.vocab30000
echo "<unk>" >> normed/normed_all.vocab30000

cat normed/normed_all.choose | awk ' { for (i=1;i<=NF;i++) cc[$i]=cc[$i]+1; } END { for (key in cc) print key,cc[key]; } ' | sort -r -n -k2 > normed/normed_all.choose.vocaball
cat normed/normed_all.choose.vocaball | head -29998 | awk ' { print $1 } ' > normed/normed_all.choose.vocab30000
echo "</s>" >> normed/normed_all.choose.vocab30000
echo "<unk>" >> normed/normed_all.choose.vocab30000
#head -200 normed_all.choose.vocab30000 > normed_all.choose.vocab200


