#!/bin/bash

set -e

DATAD="../raw/Holmes_Training_Data"

mkdir -p normed

for ff in $(ls $DATAD); do
	echo $ff
	wc $DATAD/$ff
	sleep 0.1
	./norm-single.sh $DATAD/$ff normed/${ff}_normed
	wc normed/${ff}_normed
	sleep 0.1		
done

cat normed/*_normed | awk ' (NF>2) { print $0 } ' > normed/normed_all

cat normed/normed_all | shuf | awk ' (NF > 4) && (NF < 60) { print $0 } ' > normed/normed_all.sf.len60

rm normed/*_normed 
