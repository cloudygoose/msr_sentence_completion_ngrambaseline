#!/bin/bash

set -x
set -e

NUM=200

head -$NUM normed/normed_all.choose.vocab30000 > normed/normed_all.choose.vocab$NUM

for FF in normed_all answer_normed question_normed; do
    awk ' NR==FNR { stop[$1]=1; } NR>FNR { for (i=1;i<=NF;i++) if (stop[$i]!=1) printf("%s ", $i); printf("\n");  } ' normed/normed_all.choose.vocab$NUM normed/$FF > normed/${FF}.filter
done
