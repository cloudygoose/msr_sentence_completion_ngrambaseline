import sys
import math

def main():
    print >> sys.stderr, 'Number of arguments:', len(sys.argv), 'arguments.'
    print >> sys.stderr, 'Argument List:', str(sys.argv)
    if len(sys.argv) <= 1:
        print >> sys.stderr, 'error : sys.argv <= 1 detected, Usage python cut_q_tail.py question_normed'
        sys.exit(1)
    
    txfn = sys.argv[1]

    print  >> sys.stderr, 'cutting text file ' + txfn + '...'

    line_n = 0
    cn = 5
    store = {}

    for line in open(txfn):
        line_n = line_n + 1
        choice_now = cn if (line_n % cn==0) else line_n % cn
        store[choice_now] = line.split() 
        if (choice_now == cn):
            ll = len_same(store[1], store[cn])
            for i in range(2, cn):
                if (len_same(store[i], store[cn]) != ll):
                    print >> sys.stderr, 'in function main, length not consistent.'
            for i in range(1, cn + 1):
                for j in range(ll + 1):
                    print store[i][j],
                print

def len_same(l_a, l_b):
    if (len(l_a) != len(l_b)):
        print >> sys.stderr, 'in function len_same, two length not identical.'
        sys.exit(1)
    for i in range(len(l_a)):
        if (l_a[i] != l_b[i]):
            return i
    print >> sys.stderr, 'in function len_same, no different token is found.'
    sys.exit(1)

if __name__ == "__main__":
    main()


