#!/bin/bash

set -e

TESTF_ANS=../raw/MSR_Sentence_Completion_Challenge_V1/Data/Holmes.lm_format.answers.txt
TESTF_Q=../raw/MSR_Sentence_Completion_Challenge_V1/Data/Holmes.lm_format.questions.txt
TESTF_ANSMARK=../raw/MSR_Sentence_Completion_Challenge_V1/Data/Holmes.human_format.answers.txt

cat $TESTF_ANS | sed -e 's| [A-Z]\.| |g' | tr [:upper:] [:lower:] | sed -e 's|mr\.|mr|g' -e 's|mrs\.|mrs|g' -e 's|dr\.|dr|g' -e 's| st\.| st|g' -e 's|[0-9][0-9,.]*|NNN|g' -e 's|,| <comma> |g' -e 's|;| <semi> |g' -e 's|</s>||g' -e 's|<s>||g'> normed/answer_normed
cat $TESTF_Q | sed -e 's| [A-Z]\.| |g' | tr [:upper:] [:lower:] | sed -e 's|mr\.|mr|g' -e 's|mrs\.|mrs|g' -e 's|dr\.|dr|g' -e 's| st\.| st|g' -e 's|[0-9][0-9,.]*|NNN|g' -e 's|,| <comma> |g' -e 's|;| <semi> |g' -e 's|</s>||g' -e 's|<s>||g'> normed/question_normed
cat $TESTF_ANSMARK | sed -e 's|\[||g' -e 's|\]||g' | awk ' { print $2  } ' > normed/mark_extract
cat normed/question_normed | awk ' { for (i=1;i<=NF;i++) cc[$i]=1; } END { for (key in cc) print key } ' > normed/question_normed.vocaball
