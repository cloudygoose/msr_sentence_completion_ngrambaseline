#!/bin/bash
set -e

CN=18 #I set the threshold larger than one to use as little data as possible(the choose data is prepared for slow NN training, so I want less data)

cat normed/question_normed | awk ' { for (i=1;i<=NF;i++) cc[$i]=cc[$i]+1; } END { for (key in cc) print key } ' > normed/question_normed.vocaball 
awk ' NR==FNR { vv[$1]=1;  } NR>FNR { c=0; for (i=1;i<=NF;i++) if (vv[$i] == 1) c=c+1; if (c>'"$CN"') print $0 } ' normed/question_normed.vocaball normed/normed_all > normed/normed_all.choose
wc normed/normed_all.choose
