import sys
import math

def main():
    print >> sys.stderr, 'Number of arguments:', len(sys.argv), 'arguments.'
    print >> sys.stderr, 'Argument List:', str(sys.argv)
    if len(sys.argv) <= 1:
        print >> sys.stderr, 'error : sys.argv <= 1 detected,'
        sys.exit(1)
    
    dic = {}
    dic['a'] = 1
    dic['b'] = 2 
    dic['c'] = 3
    dic['d'] = 4
    dic['e'] = 5

    scorefn = sys.argv[1]
    ansfn = sys.argv[2]
    
    numm = 1
    mark = {}
    for line in open(ansfn):
        spl = line.split()
        mark[numm] = dic[spl[0]]
        numm = numm + 1
   
    numm = 0
    max_m = {}
    ans_m = {}
    line_num = 1
    for line in open(scorefn):
        if (line_num % 5 == 1):
            numm = numm + 1
            max_m[numm] = -1000000
        s_now = float(line)
        #print s_now, numm
        if (s_now > max_m[numm]):
            max_m[numm] = s_now
            ans_m[numm] = line_num % 5
            if (ans_m[numm] == 0):
                ans_m[numm] = 5
        line_num = line_num + 1
    right_num = 0
    for i in range(1, numm + 1):
        if (ans_m[i] == mark[i]):
            right_num = right_num + 1
    print right_num, right_num * 1.0 / numm

if __name__ == "__main__":
    main()


