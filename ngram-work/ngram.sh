#BASE=normed_all #acc: 0.421
BASE=normed_all.choose #acc: 0.388
TRAINF=../preprocess_new/work/normed/$BASE
TESTF=../preprocess_new/work/normed/answer_normed
MARK=../preprocess_new/work/normed/mark_extract
QUESF=../preprocess_new/work/normed/question_normed
VOCAB=../preprocess_new/work/normed/${BASE}.vocab30000
set -x

mkdir -p work
ORDER=4

ngram-count -text $TRAINF -order $ORDER -lm work/lm.o${ORDER} -unk -wbdiscount -gt1min 1 -gt2min 1 -gt3min 1 -gt4min 1 -limit-vocab -vocab $VOCAB
ngram -lm work/lm.o$ORDER -unk -order $ORDER -ppl $TESTF -debug 2 > work/lm.o${ORDER}.ppl
tail -2 work/lm.o${ORDER}.ppl
ngram -lm work/lm.o$ORDER -unk -order $ORDER -ppl $QUESF -debug 2 > work/lm.o${ORDER}.ppl.question
cat work/lm.o${ORDER}.ppl.question | grep "logprob=" | head -5200 | awk ' { print $4 } ' > work/lm.o${ORDER}.ppl.question.extract
python msr_sc_acc.py work/lm.o${ORDER}.ppl.question.extract $MARK 2>&1 | tee work/lm.o${ORDER}.result

