TRAINF=../preprocess_new/work/normed/normed_all.filter
TESTF=../preprocess_new/work/normed/answer_normed.filter
QUESF=../preprocess_new/work/normed/question_normed.filter
MARK=../preprocess_new/work/normed/mark_extract
#VOCAB=/slwork/users/txh18/workspace/sentenceCompletion/preprocess_new/work/normed_all.choose.vocab30000
set -x

#acc: 0.473

ORDER=5

ngram-count -text $TRAINF -order $ORDER -lm work/lm.o${ORDER}.filter -unk -kndiscount -interpolate -gt3min 1 -gt4min 1 -gt5min 1 
ngram -lm work/lm.o${ORDER}.filter -unk -order $ORDER -ppl $TESTF -debug 2 > work/lm.o${ORDER}.filter.ppl
tail -2 work/lm.o${ORDER}.filter.ppl
ngram -lm work/lm.o${ORDER}.filter -unk -order $ORDER -ppl $QUESF -debug 2 > work/lm.o${ORDER}.filter.ppl.question
cat work/lm.o${ORDER}.filter.ppl.question | grep "logprob=" | head -5200 | awk ' { print $4 } ' > work/lm.o${ORDER}.filter.ppl.question.extract
python msr_sc_acc.py work/lm.o${ORDER}.filter.ppl.question.extract $MARK  2>&1 | tee work/lm.o${ORDER}.filter.result

